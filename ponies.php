<?php include "header.php" ?>

    <!-- CAROUSEL -->
    <div class="container-fluid">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <?php
                foreach ($ponies as $key => $value) {

                    $active = $key == 0 ? "active" : "";
                    echo '<div class="carousel-item ' . $active . ' ">
                    <img src="images/' . $value['picture'] . '" class="d-block w-100 h-50" alt="Pictures">
                </div>';
                } ?>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- CAROUSEL / END -->

    <!-- PRESENTATION CLUB + ETABLE.JPG -->
    <div class="card mb-3 mx-auto" style="width: 80%;">
        <div class="row no-gutters">
            <div class="col">
                <img src="images/etable.jpg" class="card-img" alt="Etable">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">Poney-Club-sur-le-Loir</h5>
                    <p class="card-text text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In volutpat, risus et vehicula aliquam, justo sapien accumsan urna, nec dictum urna velit vitae risus. Vestibulum congue, mi sit amet suscipit interdum, lacus dui eleifend felis, vitae rhoncus dolor ligula iaculis nisl. Nullam quis felis sodales, congue lacus sed, vestibulum diam. Phasellus vehicula tincidunt mollis. In suscipit dignissim diam, id accumsan tellus porta vitae.<br>
                        Donec sem ante, tempor at tortor id, lobortis congue elit. Donec ut dolor lectus. Vestibulum dapibus, est at suscipit aliquet, turpis velit fermentum quam, id feugiat est tortor et justo. In sed sem vitae ante tempor tristique. Fusce odio est, tempus a nisl eget, cursus bibendum felis. Maecenas semper dui id sem varius pellentesque. Vestibulum dictum augue ac ligula tempus, non dapibus sem pulvinar.In a fermentum sapien.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- PRESENTATION CLUB + ETABLE.JPG / END-->

    <?php include "footer.php" ?>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>