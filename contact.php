
<link rel="stylesheet" href="contact.css">

<?php include "header.php"; ?>

    <?php
        $recupNom    = isset($_POST['nom'])     && !empty($_POST['nom'])    ? $_POST['nom']    :'';
        $recupPrenom = isset($_POST['prenom'])  && !empty($_POST['prenom']) ? $_POST['prenom'] :'';
        $recupMail   = isset($_POST['mail'])    && !empty($_POST['mail'])   ? $_POST['mail']   :'';
        $recupPhone  = isset($_POST['phone'])   && !empty($_POST['phone'])  ? $_POST['phone']  :'';
        $recupCom    = isset($_POST['message']) && !empty($_POST['message'])? $_POST['message']:'';
        $total = $recupNom.$recupPrenom.$recupMail.$recupPhone.$recupCom;

        $fichier = fopen('fichier.txt', 'w');
        fwrite($fichier, $total);
        fclose($fichier);

    ?>

    <div id="formulaire">
        <form action="contact.php" method="post">
            <h2>Contact</h2>
            <input class="champ" type="text" name="nom" placeholder="Votre nom" /><br />
            <input class="champ" type="text" name="prenom" placeholder="Votre prénom" /><br />
            <input class="champ" type="email" name="mail" placeholder="Votre email" /><br />
            <input class="champ" type="text" name="phone" placeholder="Votre n° de téléphone" /><br>
            <textarea class="champ" type="text" rows="10" name="message" placeholder="Laissez un commentaire" /></textarea><br>
            <input id="valid" type="submit" value="Envoyer" />
        </form>
    </div>
